/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.banco;

/**
 *
 * @author amaury
 */
public class ContaBancaria {
    private String agencia, numeroConta, titular, cpfTitular;
    private float saldo;

    public ContaBancaria() {
    }

    public ContaBancaria(String agencia, String numeroConta, String titular, String cpfTitular) {
        this.agencia = agencia;
        this.numeroConta = numeroConta;
        this.titular = titular;
        this.cpfTitular = cpfTitular;
    }
    
    public void depositar(float valor) {
        this.saldo += valor;
    }
    
    public void sacar(float valor) {
        if (valor > this.saldo) {
            System.out.println("Saldo insuficente.");
        } else {
            this.saldo -= valor;
        }
    }
    
    public void statusConta() {
        System.out.println("Titular: " + this.titular+"\nCPF: " + this.cpfTitular +"\n"
        + "Agência: " + this.agencia + "\nNumero: " + this.numeroConta + "\n"
        + "Saldo: " + this.saldo);
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getNumeroConta() {
        return numeroConta;
    }

    public void setNumeroConta(String numeroConta) {
        this.numeroConta = numeroConta;
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public String getCpfTitular() {
        return cpfTitular;
    }

    public void setCpfTitular(String cpfTitular) {
        this.cpfTitular = cpfTitular;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    
    
}
