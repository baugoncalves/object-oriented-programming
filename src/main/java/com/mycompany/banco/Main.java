/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.banco;

import java.util.ArrayList;
import java.util.Scanner;



/**
 *
 * @author amaury
 */
public class Main {
    public static void main(String[] args) {
        int opc;
        Scanner teclado = new Scanner(System.in);
        ArrayList<ContaBancaria> listaContaBancaria = new ArrayList<ContaBancaria>();
        ContaBancaria contaBancaria;
        String ag, nConta, nContaDestino, nomeTitular, cpf;
        float valor;
        
        do {
            System.out.println("1. Cadastrar Titular\n2. Realizar Depósito");
            System.out.println("3. Sacar\n4. Listar titulares\n5. Status de uma conta");
            System.out.println("6. Transferencia\n7. Sair");
            System.out.println("Opção: ");
            opc = teclado.nextInt();
            teclado.nextLine();
            
            switch(opc) {
                case 1:
                     System.out.println("Agência: ");
                     ag = teclado.nextLine();
                     System.out.println("Número da conta: ");
                     nConta = teclado.nextLine();
                     System.out.println("Titular: ");
                     nomeTitular = teclado.nextLine();
                     System.out.println("CPF Titular: ");
                     cpf = teclado.nextLine();
                     
                     contaBancaria = new ContaBancaria(ag, nConta, nomeTitular, cpf);
                     listaContaBancaria.add(contaBancaria);

                    break;
                    
                case 2:
                    System.out.println("Valor a depositar: ");
                    valor = teclado.nextFloat();
                    teclado.nextLine();
                    System.out.println("Número da conta a depositar: ");
                    nConta = teclado.nextLine();
                    
                    for (int i=0; i<listaContaBancaria.size(); i++) {
                        if (nConta.equals(listaContaBancaria.get(i).getNumeroConta())) {
                            listaContaBancaria.get(i).depositar(valor);
                        }
                        
                    }
                    break;
                    
                case 3:
                    System.out.println("Valor a sacar: ");
                    valor = teclado.nextFloat();
                    teclado.nextLine();
                    System.out.println("Número da conta a sacar: ");
                    nConta = teclado.nextLine();
                    
                    for (int i=0; i<listaContaBancaria.size(); i++) {
                        if (nConta.equals(listaContaBancaria.get(i).getNumeroConta())) {
                            listaContaBancaria.get(i).sacar(valor);
                        }
                        
                    }
                    break;
                    
                case 4:
                    for(ContaBancaria cb:listaContaBancaria) {
                        System.out.println("Titular: " + cb.getTitular());
                    }
//                    for(int i=0;i<listaContaBancaria.size();i++) {
//                        System.out.println("Titular: " + listaContaBancaria.get(i).getTitular());
//                    }
                    break;
                    
                case 5:
                    for(ContaBancaria cb:listaContaBancaria) {
                        cb.statusConta();
                    }
//                    for(int i=0;i<listaContaBancaria.size();i++) {
//                        listaContaBancaria.get(i).statusConta();
//                    }
                    break;
                    
                case 6:
                    System.out.println("Valor a transferir: ");
                    valor = teclado.nextFloat();
                    teclado.nextLine();
                    System.out.println("Número da conta origem: ");
                    nConta = teclado.nextLine();
                    System.out.println("Número da conta destino: ");
                    nContaDestino = teclado.nextLine();
                 
                                  
                               
//                    for(int posicao=0;posicao<listaContaBancaria.size();posicao++) {
//                        if(listaContaBancaria.get(posicao).getNumeroConta().equals(nConta)) {
//                            for(int iteracao = 0; iteracao < listaContaBancaria.size(); iteracao++) {
//                                if (listaContaBancaria.get(iteracao).getNumeroConta().equals(nContaDestino)) {
//                                    listaContaBancaria.get(posicao).sacar(valor);
//                                    listaContaBancaria.get(iteracao).depositar(valor);
//                                    break;
//                                }
//                            }
//                            break;
//                        }
//                    }
                    
                    int posOrigem = -1, posDestino = -1;
                    for(int i=0;i<listaContaBancaria.size();i++) {
                        if(listaContaBancaria.get(i).getNumeroConta().equals(nConta))
                            posOrigem = i;
                        if (listaContaBancaria.get(i).getNumeroConta().equals(nContaDestino))
                            posDestino = i;
                        
                        if (posOrigem >=0 && posDestino >=0)
                            break;
                    }
                    if (posOrigem == -1)
                        System.out.println("Conta origem nao encontrada");
                    if (posDestino == -1)
                        System.out.println("Conta destino nao encontrada");
                    if (posOrigem >=0 && posDestino >=0){
                        listaContaBancaria.get(posOrigem).sacar(valor);
                        listaContaBancaria.get(posDestino).depositar(valor);
                    }
                    break;
                    
                case 7:
                    System.out.println("Saindo...");
                    break;
                    
                default:
                    System.out.println("Opção inválida");
                    
            }
            
            
        }while(opc!=7);
       
        
    }
}
